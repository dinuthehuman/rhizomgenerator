# rhizomGenerator

## Installation

Setup a MySQL database, configure the database with `generatordb.sql`. Change database credentials in the following files:

- `connect.php`, lines 3-7
- `software/smh5_class/smh5_gallery.php`, lines 19-26
