<?php
include("../functions.php");
include('../connect.php');
//requireSSL();
//ini_set('session.cookie_httponly', 1);
//ini_set('session.use_only_cookies', 1);
//ini_set('session.cookie_secure', 1);
//session_set_cookie_params(10800, '/', '.buero.io');
session_start();

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);
//Variabeln aus URL lesen
$TYPE=$_GET['type'];
if(empty($TYPE)) {$TYPE="default";}


if($_GET['action'] == 'logout') {
	$_SESSION['admin'] = false;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Administration - buero.io</title>
    <!--CSS-->
    <link href="css/basic.css" rel="stylesheet" type="text/css" />
    <?php include ("css/activeCSS.php");?>
    <!--scripts-->
    <!--<script type="text/javascript" src="../software/ckeditor/ckeditor.js"></script>-->
</head>

<body>

    <!--Navigation-->
    <div id="navigation" class="greyframe">
	<?php
	if($_SESSION['admin'] == true) {
		include ("navigation.php");
	}
	else {
		echo "Login";
	}
	?>
    </div>
    
    <!--Hauptteil-->
    <div id="main" class="greyframe">
	<?php
	if($_SESSION['admin'] == true) {
		include ("types/".$TYPE.".php");
	}
	else {
		include ("types/login.php");
	}
	?>
    </div>

</body>
<?php $con->close();?>
</html>
