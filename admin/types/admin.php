<?php
#################
#Variabeln in URL
#################

$SELID = $_GET['selid'];
$ACTION = $_GET['action'];


##############
#Konfiguration
##############

$db_table = "admins";


//Nur einen Teil der Tabelle anzeigen
//WHERE-clause (empty string für alles anzeigen)
//ex. $table_where = "WHERE catid = '".$_GET['catid']."'";
$table_where = "";
//-----hidden form für Übergabe in POST-Array nicht vergessen!---------


//Tabelle wird manuell mit Pfeilen geordnet
$table_manuell = FALSE;
if($table_manuell) {
    $table_ordnung = "ordern";
    $table_ordnung_richtung = "ASC";
}

//Tabelle wird nach Datum geordnet
$table_datum = FALSE;
if($table_datum) {
    $table_ordnung = "datumUnix";
    $table_ordnung_richtung = "DESC";
    $datumFormPraefix = "datum";
    //Suffixe: Tag -> "Day", Monat -> "Mon", Jahr -> "Year"
    
    //Zusätzliche Defs. für TimeStamp
    $datumHour = 23;
    $datumMin = 59;
    $datumSec = 59;
}

//Tabelle wird nach Alphabet geordnet
$table_alpha = TRUE;
if($table_alpha) {
    $table_ordnung = "usr";
    $table_ordnung_richtung = "ASC";
}



############################
#Formulareingaben bearbeiten
############################

switch($ACTION) {
    
    
    //neuer Eintrag od. Eintrag aktualisieren
    case "modify":
	
		if($_POST['pwd1'] == $_POST['pwd2'] && strlen($_POST['pwd1']) > 4) {
				$_POST['pwd_hash'] = hash('sha256', $_POST['pwd1']);
				//Alle MySQL-Spalten auflisten und mit POST-Array vergleichen
				$result = $con->query("SHOW COLUMNS FROM $db_table");
				while ($row = mysqli_fetch_assoc($result)) {
					$tablefields[] = $row['Field'];
				}
				
				foreach($tablefields as $value) {
					if(isset($_POST[$value])) {
					$relevantFields[] = $value;
					$relevantValues[$value] = $_POST[$value];
					}
				}
				
				
				//NEUER EINTRAG
				if(empty($SELID)) { 
					
					if($table_manuell) {
					//neue Ordnungsnummer
					$sql = "SELECT MAX($table_ordnung) FROM $db_table $table_where";
					$result = $con->query($sql);
					while($row = mysqli_fetch_array($result)){
						$ordern1=$row["MAX($table_ordnung)"]+1;
					}
					}
					
					//Daten schreiben -> Eröffnung MySQL
					$sql1 = "INSERT INTO $db_table (";
					$sql2 = ") VALUES (";
					
					//neue Ordnungsnummer eintragen bei manueller Sortierung
					if($table_manuell) { 
					$sql1 .= "$table_ordnung,";
					$sql2 .= "'$ordern1',";
					}
					
					//UNIX-timestamp eintragen bei Datum-Sortierung
					if($table_datum) { 
					$datumTemp = mktime($datumHour,$datumMin,$datumSec,$_POST[$datumFormPraefix.'Mon'],$_POST[$datumFormPraefix.'Day'],$_POST[$datumFormPraefix.'Year']);
					$sql1 .= "$table_ordnung,";
					$sql2 .= "'$datumTemp',";
					}
					
					//der Tabelle entsprechende Daten aus dem POST-Array eintragen
					for($n = 0; $n < count($relevantFields); $n++) {
					$sql1 .= $relevantFields[$n];
					$sql2 .= "'".$relevantValues[$relevantFields[$n]]."'";
					if($n != count($relevantFields) -1) {
						$sql1 .= ',';
						$sql2 .= ',';
					}
					}
					$sql = $sql1.$sql2.")";
					$result = $con->query($sql);
				}
			
			
			//EINTRAG AKTUALISIEREN
			else { 
				$sql = "UPDATE $db_table SET ";
				
				//Datum aktualisiern, falls nach Datum geordnet
				if($table_datum) {
				$datumTemp = mktime($datumHour,$datumMin,$datumSec,$_POST[$datumFormPraefix.'Mon'],$_POST[$datumFormPraefix.'Day'],$_POST[$datumFormPraefix.'Year']);
				$sql .= "$table_ordnung = '$datumTemp',";
				}
				
				//andere Daten aus dem POST-Array aktualisieren
				for($n = 0; $n < count($relevantFields); $n++) {
				$sql .= $relevantFields[$n]."='".$relevantValues[$relevantFields[$n]]."'";
				if($n != count($relevantFields) -1) {
					$sql .= ',';
				}
				}
				$sql .= " WHERE id = '$SELID'";
				$result = $con->query($sql);
			}
			
			$fbmsg = "Neues Passwort erfasst.";
		}
		
		else {
			$fbmsg = "Passwort zu kurz oder mit Wiederholung nicht identisch.";
		}
		break;
    
    
    //Einträge löschen
    case "delete": 
    
    for($n = 0; $n < $_POST['nCheck']; $n++) {
        $delcheck[$n] = $_POST["delcheck".$n];
    }
    
    foreach($delcheck as $value) {
        $sql = "DELETE FROM $db_table WHERE id='$value'";
        $result=$con->query($sql);
    }
    
    break;
    
    
    //Eintrag verschieben
    case "move": 
    
    $sql="UPDATE $db_table SET $table_ordnung='".$_GET['to1']."' WHERE id='".$_GET['id1']."'";
    $result = $con->query($sql);
    $sql="UPDATE $db_table SET $table_ordnung='".$_GET['to2']."' WHERE id='".$_GET['id2']."'";
    $result = $con->query($sql);
    
    break;
}



##################
#Tabelle auflisten
##################

//Vorbereitung für moveup/movedown -> Alle ids und Ordnungsnummern speichern
if($table_manuell) {
    $sql="SELECT id,ordern FROM $db_table $table_where ORDER BY $table_ordnung $table_ordnung_richtung";
    $result = $con->query($sql);
    while($row = mysqli_fetch_array($result)) {
    $ALLordern[]=$row[$table_ordnung];
    $ALLid[]=$row['id'];
    }
}

//Einfärben der gewählten Tabellen-Zeile - Definition
$activeStyleName='STYLE'.$SELID;
$$activeStyleName=$activeStyleTableBg;
?>

<!--Titel-->
<?php
if(!empty($fbmsg)) {
	echo '<h6>';
	echo $fbmsg;
	echo '</h6>';
}
?>
<h1>Administratoren</h1>

<!--Header der Tabelle-->
<table id="liste"><form action="<?php echo changeParam(currURL(),'action','delete'); ?>" method="post">
    <tr>
    <th style="width: 16px;"></th>
    <th>User</th>
    <th style="width: 60px;">löschen</th>
    </tr>
<?php

//MySQL alle Zeilen auswählen
$sql="SELECT * FROM $db_table $table_where ORDER BY $table_ordnung $table_ordnung_richtung";
$result = $con->query($sql);
if(!$result) {die(mysqli_error());}

//Variable für Zählen der Zeilen öffnen (nötig für "Löschen"-Spalte)
$nCheck=0;

//alle Tabellenzeilen scheiben
while($row=mysqli_fetch_array($result)) {
    
    //Zahlenwerte für moveup/movedown speichern
    if($table_manuell) {
    $moveup=movenumbers($ALLid,$ALLordern,-1,$row['id']);
    $movedown=movenumbers($ALLid,$ALLordern,1,$row['id']);
    }
    
    //Style für eingefärbte Zeile zusammensetzen
    $actStyleBg='STYLE'.$row['id'];
    ?>
    
    <!--HTML Tabellenzeile-->
    <tr style="<?php echo $$actStyleBg; ?>">
    <td>
        <a href="<?php echo changeParam(currURL(),'selid',$row['id'],'action','0'); ?>"><img src="img/edit.png">
    </td>
    <td>
        <?php echo $row['usr']; ?>
    </td>
    <td>
        <input type="checkbox" name="delcheck<?php echo $nCheck; ?>" value="<?php echo $row['id']; ?>">
    </td>
    </tr>
    
    <?php
    //Zeile zählen (für Löschen-Formular)
    $nCheck++;
}

//Zeile für "neuen Eintrag" und "löschen" Button
?>
    <tr>
    <td colspan="2" style="border: none;"><a href="<?php echo unsetParam(currURL(),'selid','action'); ?>">neuen Eintrag erfassen</a></td>
    <td colspan="1" style="border: none; text-align: right;">
    <input type="hidden" name="nCheck" value="<?php echo $nCheck; ?>" />
    <input type="submit" value="markierte Einträge löschen"/></td>
    </tr>

<!--Tabelle & Löschen-Formular schliessen-->
</form></table>
<?php



###################################
#Forumlar zur Bearbeitung der Daten
###################################

//Falls Änderung (und nicht Neueintrag), zu ändernde Daten laden
if(!empty($SELID)) {
    $sql="SELECT * FROM $db_table WHERE id='$SELID'";
    $result = $con->query($sql);
    $row=mysqli_fetch_array($result);
}

//Formular für Änderung/Neueintrag der Daten
?>

<form action="<?php echo changeParam(currURL(),'action','modify'); ?>" method="post" id="aktedit">
    Username:<br />
    <input type="text" id="usr" name="usr" class="fill" value="<?php if(!empty($SELID)) {echo $row['usr'];} ?>" /><br />
	
	Neues Passwort:
	<input type="password" id="pwd1" name="pwd1" class="fill" value="" /><br />
	
	Neues Passwort wiederholen:
	<input type="password" id="pwd2" name="pwd2" class="fill" value="" /><br />
    
    <input type="submit" value="<?php if(empty($SELID)){echo 'Eintrag erfassen';} else {echo 'Eintrag ändern';}?> " />
</form>