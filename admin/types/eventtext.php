<?php
#################
#Variabeln in URL
#################

$SELID = $_GET['selid'];
$ACTION = $_GET['action'];


##############
#Konfiguration
##############

$db_table = "textseiten";


//Nur einen Teil der Tabelle anzeigen
//WHERE-clause (empty string für alles anzeigen)
//ex. $table_where = "WHERE catid = '".$_GET['catid']."'";
$table_where = "WHERE catid = '".$_GET['catid']."' AND is_event = '".$_GET['is_event']."'";
//-----hidden form für Übergabe in POST-Array nicht vergessen!---------


//Tabelle wird manuell mit Pfeilen geordnet
$table_manuell = !$_GET['is_event'];
if($table_manuell) {
    $table_ordnung = "ordern";
    $table_ordnung_richtung = "ASC";
}

//Tabelle wird nach Datum geordnet
$table_datum = $_GET['is_event'];
if($table_datum) {
    $table_ordnung = "verfallsdatum";
    $table_ordnung_richtung = "DESC";
    $datumFormPraefix = "datum";
    //Suffixe: Tag -> "Day", Monat -> "Mon", Jahr -> "Year"
    
    //Zusätzliche Defs. für TimeStamp
    $datumHour = 23;
    $datumMin = 59;
    $datumSec = 59;
}

//Tabelle wird nach Alphabet geordnet
$table_alpha = FALSE;
if($table_alpha) {
    $table_ordnung = "titel";
    $table_ordnung_richtung = "ASC";
}



############################
#Formulareingaben bearbeiten
############################

switch($ACTION) {
    
    
    //neuer Eintrag od. Eintrag aktualisieren
    case "modify":
	
    //Alle MySQL-Spalten auflisten und mit POST-Array vergleichen
    $result = $con->query("SHOW COLUMNS FROM $db_table");
    while ($row = mysqli_fetch_assoc($result)) {
        $tablefields[] = $row['Field'];
    }
    
    foreach($tablefields as $value) {
        if(isset($_POST[$value])) {
        $relevantFields[] = $value;
        $relevantValues[$value] = $_POST[$value];
        }
    }
    
    
    //NEUER EINTRAG
    if(empty($SELID)) { 
        
        if($table_manuell) {
        //neue Ordnungsnummer
        $sql = "SELECT MAX($table_ordnung) FROM $db_table $table_where";
        $result = $con->query($sql);
        while($row = mysqli_fetch_array($result)){
            $ordern1=$row["MAX($table_ordnung)"]+1;
        }
        }
        
        //Daten schreiben -> Eröffnung MySQL
        $sql1 = "INSERT INTO $db_table (";
        $sql2 = ") VALUES (";
        
        //neue Ordnungsnummer eintragen bei manueller Sortierung
        if($table_manuell) { 
        $sql1 .= "$table_ordnung,";
        $sql2 .= "'$ordern1',";
        }
        
        //UNIX-timestamp eintragen bei Datum-Sortierung
        if($table_datum) { 
        $datumTemp = mktime($datumHour,$datumMin,$datumSec,$_POST[$datumFormPraefix.'Mon'],$_POST[$datumFormPraefix.'Day'],$_POST[$datumFormPraefix.'Year']);
        $sql1 .= "$table_ordnung,";
        $sql2 .= "'$datumTemp',";
        }
        
        //der Tabelle entsprechende Daten aus dem POST-Array eintragen
        for($n = 0; $n < count($relevantFields); $n++) {
        $sql1 .= $relevantFields[$n];
        $sql2 .= "'".$relevantValues[$relevantFields[$n]]."'";
        if($n != count($relevantFields) -1) {
            $sql1 .= ',';
            $sql2 .= ',';
        }
        }
        $sql = $sql1.$sql2.")";
        $result = $con->query($sql);
    }
    
    
    //EINTRAG AKTUALISIEREN
    else { 
        $sql = "UPDATE $db_table SET ";
        
        //Datum aktualisiern, falls nach Datum geordnet
        if($table_datum) {
        $datumTemp = mktime($datumHour,$datumMin,$datumSec,$_POST[$datumFormPraefix.'Mon'],$_POST[$datumFormPraefix.'Day'],$_POST[$datumFormPraefix.'Year']);
        $sql .= "$table_ordnung = '$datumTemp',";
        }
        
        //andere Daten aus dem POST-Array aktualisieren
        for($n = 0; $n < count($relevantFields); $n++) {
        $sql .= $relevantFields[$n]."='".$relevantValues[$relevantFields[$n]]."'";
        if($n != count($relevantFields) -1) {
            $sql .= ',';
        }
        }
        $sql .= " WHERE id = '$SELID'";
        $result = $con->query($sql);
    }
    
    break;
    
    
    //Einträge löschen
    case "delete": 
    
    for($n = 0; $n < $_POST['nCheck']; $n++) {
        $delcheck[$n] = $_POST["delcheck".$n];
    }
    
    foreach($delcheck as $value) {
        $sql = "DELETE FROM $db_table WHERE id='$value'";
        $result=$con->query($sql);
    }
    
    break;
    
    
    //Eintrag verschieben
    case "move": 
    
    $sql="UPDATE $db_table SET $table_ordnung='".$_GET['to1']."' WHERE id='".$_GET['id1']."'";
    $result = $con->query($sql);
    $sql="UPDATE $db_table SET $table_ordnung='".$_GET['to2']."' WHERE id='".$_GET['id2']."'";
    $result = $con->query($sql);
    
    break;
}



##################
#Tabelle auflisten
##################

//Vorbereitung für moveup/movedown -> Alle ids und Ordnungsnummern speichern
if($table_manuell) {
    $sql="SELECT id,ordern FROM $db_table $table_where ORDER BY $table_ordnung $table_ordnung_richtung";
    $result = $con->query($sql);
    while($row = mysqli_fetch_array($result)) {
    $ALLordern[]=$row[$table_ordnung];
    $ALLid[]=$row['id'];
    }
}


//Einfärben der gewählten Tabellen-Zeile - Definition
$activeStyleName='STYLE'.$SELID;
$$activeStyleName=$activeStyleTableBg;
?>


<!--Titel-->
<h1>
<?php
$xresult = $con->query("SELECT linktext_de, catid FROM menus WHERE id=".$_GET['catid']);
$xrow = mysqli_fetch_array($xresult);
$catrow = $xrow;
if($_GET['is_event']){$h1 = "Events ";}
else {$h1 = "Artikel ";}
$h1 .= '"'.$xrow['linktext_de'].'"';
while($xrow['catid'] != 0) {
	$xresult = $con->query("SELECT linktext_de, catid FROM menus WHERE id=".$xrow['catid']);
	$xrow = mysqli_fetch_array($xresult);
	$h1 = $xrow['linktext_de'].' > '.$h1;
}

echo "Hauptmenu > ".$h1;
?>

</h1>

<!--Header der Tabelle-->
<table id="liste"><form action="<?php echo changeParam(currURL(),'action','delete'); ?>" method="post">
    <tr>
    <th style="width: 16px;"><a href="<?php echo changeParam(currUrl(), 'type','menu', 'catid', $catrow['catid']); ?>"><img src="img/parent.png"></a></th>
	<th style="width: 16px;"></th>
    <th>Titel</th>
    <th style="width: 60px;"></th>
    <th style="width: 60px;">löschen</th>
    </tr>
<?php

//MySQL alle Zeilen auswählen
$sql="SELECT * FROM $db_table $table_where ORDER BY $table_ordnung $table_ordnung_richtung";
$result = $con->query($sql);
if(!$result) {die(mysqli_error());}

//Variable für Zählen der Zeilen öffnen (nötig für "Löschen"-Spalte)
$nCheck=0;

//alle Tabellenzeilen scheiben
while($row=mysqli_fetch_array($result)) {
    
    //Zahlenwerte für moveup/movedown speichern
    if($table_manuell) {
    $moveup=movenumbers($ALLid,$ALLordern,-1,$row['id']);
    $movedown=movenumbers($ALLid,$ALLordern,1,$row['id']);
    }
    
    //Style für eingefärbte Zeile zusammensetzen
    $actStyleBg='STYLE'.$row['id'];
    ?>
    
    <!--HTML Tabellenzeile-->
    <tr style="<?php echo $$actStyleBg; ?>">
    <td>
        <a href="<?php echo changeParam(currURL(),'selid',$row['id'],'action','0'); ?>"><img src="img/edit.png">
    </td>
	<td>
        <a href="index.php?type=gallery&galid=<?php echo $row['id']; ?>"><img src="img/camera.png">
    </td>
    <td>
        <?php echo $row['titel_de']; ?>
    </td>
    <td>
        <?php if($table_manuell) { //manuelle Sortierung, Pfeile anzeigen ?>
        <a href="<?php echo changeParam(currURL(),'action','move','id1',$moveup[0],'to1',$moveup[1],'id2',$moveup[2],'to2',$moveup[3]); ?>"><img src="img/moveup.png"/></a>
        <a href="<?php echo changeParam(currURL(),'action','move','id1',$movedown[0],'to1',$movedown[1],'id2',$movedown[2],'to2',$movedown[3]); ?>"><img src="img/movedown.png"/></a>
        <?php }
        
        if($table_datum) { //Datum-Sortierung, Datum anzeigen
        echo date("d.m.Y",$row[$table_ordnung]);
        } ?>
    </td>
    <td>
        <input type="checkbox" name="delcheck<?php echo $nCheck; ?>" value="<?php echo $row['id']; ?>">
    </td>
    </tr>
    
    <?php
    //Zeile zählen (für Löschen-Formular)
    $nCheck++;
}

//Zeile für "neuen Eintrag" und "löschen" Button
?>
    <tr>
    <td colspan="3" style="border: none;"><a href="<?php echo unsetParam(currURL(),'selid','action'); ?>">neuen Eintrag erfassen</a></td>
    <td colspan="2" style="border: none; text-align: right;">
    <input type="hidden" name="nCheck" value="<?php echo $nCheck; ?>" />
    <input type="submit" value="markierte Einträge löschen"/></td>
    </tr>

<!--Tabelle & Löschen-Formular schliessen-->
</form></table>
<?php



###################################
#Forumlar zur Bearbeitung der Daten
###################################

//Falls Änderung (und nicht Neueintrag), zu ändernde Daten laden
if(!empty($SELID)) {
    $sql="SELECT * FROM $db_table WHERE id='$SELID'";
    $result = $con->query($sql);
    $row=mysqli_fetch_array($result);
}

//Formular für Änderung/Neueintrag der Daten
?>

<form action="<?php echo changeParam(currURL(),'action','modify'); ?>" method="post" id="aktedit">
    
	<input type="hidden" name="gal_overlay" value="0" />
	<input type="checkbox" name="gal_overlay" value="1" <?php if($row['gal_overlay'] == 1 || empty($SELID)) echo "checked='checked'"; ?>/> Bildergalerie als Galerie verwenden (anstatt nur für den Würfel)
	<br /><br />
	
	Titel deutsch:<br />
    <textarea id="" name="titel_de" class="ckeditor"><?php if(!empty($SELID)) {echo $row['titel_de'];} ?></textarea><br />
	
	Titel englisch:<br />
    <textarea id="" name="titel_en" class="ckeditor"><?php if(!empty($SELID)) {echo $row['titel_en'];} ?></textarea><br />
	
	<?php if($_GET['is_event']) { ?>
    Kurztext deutsch:<br />
    <textarea id="" name="eventshort_de" class="ckeditor"><?php if(!empty($SELID)) {echo $row['eventshort_de'];} ?></textarea><br />
	
	Kurztext englisch:<br />
    <textarea id="" name="eventshort_en" class="ckeditor"><?php if(!empty($SELID)) {echo $row['eventshort_en'];} ?></textarea><br />
    <?php } ?>
	
	Text deutsch:<br />
    <textarea id="" name="text_de" class="ckeditor"><?php if(!empty($SELID)) {echo $row['text_de'];} ?></textarea><br />
	
	Text englisch:<br />
    <textarea id="" name="text_en" class="ckeditor"><?php if(!empty($SELID)) {echo $row['text_en'];} ?></textarea><br />
	
	<?php if($_GET['is_event']) { ?>
    <div class="inlineedit">
		Verfallsdatum: 
		<input type="text" class="zweiZahlen" name="datumDay" value="<?php if(!empty($SELID)) {echo date("d",$row['verfallsdatum']);} ?>"/>.
        <input type="text" class="zweiZahlen" name="datumMon" value="<?php if(!empty($SELID)) {echo date("m",$row['verfallsdatum']);} ?>"/>.
        <input type="text" class="vierZahlen" name="datumYear" value="<?php if(!empty($SELID)) {echo date("Y",$row['verfallsdatum']);} ?>"/>
    </div>
	
	
	<input type="hidden" name="event_doc" value="0" />
	<input type="checkbox" name="event_doc" value="1" <?php if($row['event_doc'] == 1 || empty($SELID)) echo "checked='checked'"; ?>/> Event dokumentieren <br />
	<br /><br />
    <?php } ?>
    <input type="hidden" name="catid" value="<?php echo $_GET['catid']; ?>" />
	<input type="hidden" name="is_event" value="<?php echo $_GET['is_event']; ?>" />
    
    <input type="submit" value="<?php if(empty($SELID)){echo 'Eintrag erfassen';} else {echo 'Eintrag ändern';}?> " />
</form>