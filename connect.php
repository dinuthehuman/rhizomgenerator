<?php

$db_host = 'db_host';
$db_user = 'db_user';
$db_pwd = 'db_pwd';

$database = 'generatordb';

$con = new mysqli($db_host, $db_user, $db_pwd, $database);
if ($con->connect_error) {
    die('Connect Error (' . $con->connect_errno . ') '
            . $con->connect_error);
}
$con->set_charset("utf8");
?>
