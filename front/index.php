<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Image Generator</title>
    <script src="processing.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        function img_to_serv(str64) {
            $.post("push.php",
            {
                img: str64,
            },
            function(data, status){
                window.open("http://46.101.195.157/generator/front/" + data, "_blank");
                return data;

            });
        }
    </script>
</head>

<body>
    <script type="application/processing">
    
    var img_array = <?php include("pullarray.php"); ?>;
    console.log(typeof(img_array));
    
    PImage imgtx;
    PImage img;
    PGraphics imgMix;
    PGraphics imgOut;
    PGraphics canvas;
    PFont f;
    
    
    HScrollbar hs1, hs2, hs3, hs4,hs5;  // sliders
    HScrollbar[] sliderCollection= new HScrollbar[4];
    
    Knob k0,k1,k2,k3,k4;
    Knob[] knobCollection= new Knob[4];
    
    boolean act=false;
    
    
    int unit=10;
    int sx, sy; //size of sampled width and height
    
    float hv=0.5;
    
    float fa=0;
    float fb=0;
    
    boolean controlon;
    
    float br;
    
    float [] brightlist;
    char [] charlist;
    
    boolean saveBool=false;
    boolean done=false;
    
    PImage[] loadedImg;
    PImage downloadB;
    
    int randomPick, randomPickB;
    
    void setup() {
    
      size(1240, 1748);
      
     // size(670, 1000);
      
      background(255);
      fill(0);
      //f = loadFont("GTPressuraMono-5.vlw");
      //textFont(f);
    
      //imgtx = loadImage("smh5/resz/1-11.jpg");            ///this
      //imgtx = loadImage("smh5/resz/1-12.jpg");
      //img = loadImage(img_array[15]);            ///this
      //console.log(img_array);
      
      loadedImg= new PImage[img_array.length];
      
      
      for(int i=0; i<img_array.length; i++){
     
     loadedImg[i]= loadImage(img_array[i]);
     
     
      //println(img_array[i]);
      }
      
      
      
      downloadB=loadImage("download.png")
    
      canvas=createGraphics(650, 800);
      
      imgMix=createGraphics(1240, 1748);
      imgOut=createGraphics(1240, 1748);
    
      ////////SLIDERS
    
      hs1 = new HScrollbar(0, 819, canvas.width, 35, 2);
      hs2 = new HScrollbar(0, 853, canvas.width, 35, 2);
      hs3 = new HScrollbar(0, 887, canvas.width, 35, 2);
      hs4 = new HScrollbar(0, 921, canvas.width, 35, 2);
     // hs5 = new HScrollbar(0, 954, canvas.width, 35, 2);
      
      
        sliderCollection[0]=hs1;
        sliderCollection[1]=hs2;
        sliderCollection[2]=hs3;
        sliderCollection[3]=hs4;
      //  sliderCollection[4]=hs5;        
      
      
      
     /* 
      k0= new Knob(100,50,0);
      knobCollection[0]=k0;
      
      k1= new Knob(150,50,0);
      knobCollection[1]=k1; 
      
      k2= new Knob(200,50,0);
      knobCollection[2]=k2; 
      
      k3= new Knob(250,50,0);
      knobCollection[3]=k3;
      
      k4= new Knob(300,50,0);
      knobCollection[4]=k4;
      */
     
      
      randomPick= floor(random(img_array.length));
      randomPickB= floor(random(5,img_array.length-5));
      
      
    }
    
    
    
    void draw() {
    
    background(0);
    
       for (int i=0; i<sliderCollection.length; i++) {
         HScrollbar hst= sliderCollection[i];
         hst.update();         
      }
    
    /*
    for (int i=0; i<knobCollection.length; i++) {
         Knob kn= knobCollection[i];
         kn.update();         
      }
    */
    
    
    int indexa = floor(map(sliderCollection[0].getPos(),0,1,1,img_array.length));
  console.log(img_array[0]);
    
    
  img = loadedImg[(indexa+randomPick)%img_array.length];            ///this
 
  imgtx = loadedImg[(indexa+randomPick+randomPickB)%loadedImg.length]; 
    
   // img = loadImage(img_array[indexa]); 
    
    
   // imgtx = loadImage(img_array[(indexa+(img_array.length/2))%img_array.length]); 
    
    
    
   int alph=floor(map(sliderCollection[1].getPos(),0,1,0,255));
   // int alph=100;
    
    
    
      unit=floor(map(sliderCollection[2].getPos(),0,1,8,20));
      
    // unit=20;
     
     
     
     //  hv=0.5;
       
       hv=map(sliderCollection[3].getPos(),0,1,0,1);
     
     
     
     
     
     
     
     
     
      sx=imgMix.width/unit;
      sy=imgMix.height/unit;
  
      fa=0.7;
      fb=0.7;
      
      
     // fa=map(sliderCollection[0].getPos(),0,1,1.2,0.6);
     // fb=map(sliderCollection[4].getPos(),0,1,1.2,0.6);
      
   
    
    
      imgMix.beginDraw();
      imgMix.background(255);
      imgMix.tint(255, 255);
      imgMix.image(img, 0, 0);
      imgMix.tint(255, alph);
      imgMix.image(imgtx, 0, 0);
      imgMix.endDraw();
    
    
      brightlist= new float[sx*sy];
    
      imgOut.beginDraw();
      imgOut.background(0);
      horStyle();
      imgOut.endDraw();
      
     
     
     
      canvas.beginDraw();
      canvas.background(0);
      canvas.fill(0);
      canvas.noStroke();
      canvas.smooth();
      //canvas.scale(0.6178489702517162);
      canvas.scale(650/1240);
      canvas.image(imgOut, 0, -100);
      
      //canvas.scale(1.618518518518519);
      canvas.scale(1240/650);
    
    //canvas.fill(255);
    
      if (mouseX>canvas.width-90 && mouseX<(canvas.width) && mouseY>970 && mouseY<989) {
   
        tint(150);
        //canvas.fill(255);
        
      } else {
        tint(255);
        //canvas.fill(150);
      }
    
      //canvas.rectMode(CORNER);
      //canvas.rect(277,964, 90, 20);
      image(downloadB,canvas.width-90,964);

      tint(255);
   
      
      
      
     
      for (int i=0; i<sliderCollection.length; i++) {    
         HScrollbar hst= sliderCollection[i];
         hst.display();     
      }
     
    /*
     for (int i=0; i<knobCollection.length; i++) {
         Knob kn= knobCollection[i];
         kn.display();         
      }
    */
    
    
    
      canvas.endDraw();
    
    
      if (saveBool==true) {
      background(0);
      image(imgOut,0,0);
        //UPLOAD
        var leinwand = document.getElementById('sketch');
        console.log(document.getElementById('sketch'));
        var dataURL = leinwand.toDataURL();
        
        //redirect_to = img_to_serv(dataURL);
        img_to_serv(dataURL);
        //saveFrame("dataoutC/line-######.png");
        
      }
    
      
      image(canvas, 0, 0);
    
      saveBool=false;
    }
    
    
    
    
    
    
    
    void mouseClicked() {
    
      if (mouseX>canvas.width-90 && mouseX<(canvas.width) && mouseY>970 && mouseY<989) {
        saveBool=true;
      }
    } 
  
  
    void mousePressed() {


        for (int i=0; i<sliderCollection.length; i++) {
            HScrollbar hst= sliderCollection[i];
            if (hst.over) {
                hst.select= true;
                controlon=true;
            }
        }
        
        
        ///////////////////////////////
/*
        for (int i=0; i<knobCollection.length; i++) {
            Knob kn= knobCollection[i];
            if (kn.over) {
                kn.select= true;
                //controlon=true;
            }
            }
        */
    }


    void mouseReleased() {

        for (int i=0; i<sliderCollection.length; i++) {     
            HScrollbar hst= sliderCollection[i];
            hst.select=false;
        }
        
        
        ///////////////////
        /*
        for (int i=0; i<knobCollection.length; i++) {     
            Knob kn= knobCollection[i];
            kn.select=false;
        }
        */
       
        
    } 
  
  
  
  
    
    
    
    ////////////////////////////////////////////////////////////////////////GRAPHIC FUNCTION
    
    
    void horStyle(){
    
    imgMix.loadPixels();
       for (int i=0; i<sx; i++) {
        for (int j=0; j<sy; j++) {
    
          br = brightness(imgMix.pixels[(j*unit*imgMix.width)+i*unit]);
          //br=random(0,255);
          brightlist[(j*sx)+i] = br;
    
    
          imgOut.fill(0);
          imgOut.rectMode(CORNER);
          imgOut.stroke(0);
          imgOut.strokeWeight(1);
 
          float fbb=fb;
            
         // if(controlon==true){
         //   fbb=fb;
         // }else{
            fbb=fb*random(0.1,unit/8);
         // }
         
        //float faa=fa*random(0.1,unit/8);
        float faa=fa+(frameCount)%unit;
 
          if (br<=255*hv) {
          
            imgOut.noStroke();
            imgOut.fill(255);     
            imgOut.rect((i*unit), (j*unit)+fbb, unit, map(br, 0, 255, 0, unit*fb));
    
    
          }else {
      
    
            imgOut.noStroke();
            imgOut.fill(255);
            //println(fa);
           imgOut.rect((i*unit)+faa, (j*unit), map(br, 0, 255, 0, unit*fa), unit );
            
           // imgOut.rect((i*unit), (j*unit), map(br, 0, 255, 0, faa), unit );
            
    imgOut.fill(0);
        }
      } 
    }
    imgMix.updatePixels();
    
    }
    
    
    //////////////////////////////////////////////////////////////////SLIDERS
    
    
   
    class HScrollbar {
        int swidth, sheight;    // width and height of bar
        float xpos, ypos;       // x and y position of bar
        float spos, newspos;    // x position of slider
        float sposMin, sposMax; // max and min values of slider
        int loose;              // how loose/heavy
        boolean over;           // is the mouse over the slider?
        boolean select;
        boolean locked;
        float ratio;

        HScrollbar (float xp, float yp, int sw, int sh, int l) {
            swidth = sw;
            sheight = sh;
            int widthtoheight = sw - sh;
            ratio = (float)sw / (float)widthtoheight;
            xpos = xp;
            ypos = yp;
            //spos = xpos + swidth/2 - sheight/2;
            spos = xpos+20;
            newspos = spos;
            sposMin = xpos;
            sposMax = xpos + swidth - sheight;
            loose = l;
        }

  void update() {

    


    if (overEvent()) {
      over = true;
    } else {
      over = false;
    }
    if (mousePressed && select) {
      locked = true;
    }
    if (!mousePressed) {
      locked = false;
    }
    if (locked) {
      newspos = constrain(mouseX-sheight/2, sposMin, sposMax);
    }
    if (abs(newspos - spos) > 1) {
      spos = spos + (newspos-spos)/loose;
    }
  }

  float constrain(float val, float minv, float maxv) {
    return min(max(val, minv), maxv);
  }

  boolean overEvent() {
    if (mouseX > xpos && mouseX < xpos+swidth &&
      mouseY > ypos && mouseY < ypos+sheight ) {
      return true;
    } else {
      return false;
    }
  }


  
  void display() {
    stroke(255);
    strokeWeight(1);
    fill(0);
    rect(xpos, ypos, swidth,  17);
    if (over || locked) {
      noStroke();
      fill(255);
    } else {
      noStroke();
      fill(255);
    }
      rect(spos, ypos, sheight+1, 17);
  }

  float getPos() {
    // Convert spos to be values between
    // 0 and the total width of the scrollbar
    return map(spos * ratio,sposMin* ratio, sposMax* ratio,0,1);
  }
}
    
   
   
    
//////////////////////////////////////////////////////////////////////////////////    
    
/*    
    
    
class Button {

  float xpos, ypos;
  float sidex=30;
  float sidey=15;
  int id;

  boolean over;          
  boolean select;
  boolean locked;

  Button(float _xpos, float _ypos, int _id) {

    xpos=_xpos;
    ypos=_ypos;
    id=_id;
  }


  void update() {

    if (overEvent()) {

      over = true;
    } else {
      over = false;
    }
    if (mousePressed && select) {

      locked = true;
      
      for (int i=0; i<buttonCollection.length; i++){
       
        if(i!=id){
        
        Button bt= buttonCollection[i];
        bt.locked=false;
        
        }    
      }
    }
  }



  boolean overEvent() {
    if (mouseX > xpos && mouseX < xpos+sidex &&
      mouseY > ypos && mouseY < ypos+sidey ) {
      return true;
    } else {
      return false;
    }
  }



  void display() {

    if (locked==true) {
        canvas.fill(0);
    }else {
        canvas.fill(255);
    }

canvas.rect(xpos, ypos, sidex, sidey);

}
}    
    
    
  */  
    
   /* 
    
   class Knob {

  int rout=30;
  int rin=18;    // width and height of bar
  float xpos, ypos;       // x and y position of bar
  float spos=PI;
  float sposmin=0;
  float sposmax=PI*2;
  float newspos;    // x position of slider
  int loose;              // how loose/heavy
  boolean over;           // is the mouse over the slider?
  boolean select;
  boolean locked;


  Knob(float xp, float yp, int l) {

    xpos=xp;
    ypos=yp;
    loose=l;
  }


  void update() {

    println(newspos);


    if (overEvent()) {

      over = true;
    } else {
      over = false;
    }
    if (mousePressed && select) {

      locked = true;
    }
    if (!mousePressed) {
      locked = false;
    }
    if (locked) {
      println("now");
      newspos = spos+map(mouseY, ypos, width, 0, 2*PI);
      newspos = newspos+map(mouseX, xpos, width, 0, 2*PI);
      newspos = constrain(newspos, sposmin, sposmax);
    }
    if (select==false) {
      spos= newspos;
    }
    if (abs(newspos - spos) > 1) {
      //   spos = spos + (newspos-spos)/loose;
    }

    println("newspos: " + newspos);
  }

  float constrain(float val, float minv, float maxv) {
    return min(max(val, minv), maxv);
  }

  boolean overEvent() {
    if (mouseX > xpos-rin && mouseX < xpos+rin &&
      mouseY > ypos-rin && mouseY < ypos+rin ) {
      return true;
    } else {
      return false;
    }
  }

  void display() {

if(locked){
    canvas.fill(255);
    }else{
    
    canvas.fill(220);
    }
    canvas.noStroke();

    canvas.ellipse(xpos, ypos, (rin+3)*2, (rin+3)*2);

    canvas.fill(255);

   // canvas.arc(xpos, ypos, rin*2, rin*2, -PI/2, newspos-PI/2, PIE);

canvas.stroke(0);
canvas.strokeWeight(5);
canvas.point(xpos+((rin-2)*sin(-newspos-PI)), ypos+((rin-2)*cos(-newspos-PI)));

    canvas.strokeWeight(2);
    canvas.stroke(0);
    canvas.point(xpos, ypos);
    canvas.line(xpos, ypos, xpos, ypos-rin);

    canvas.strokeWeight(25);
    canvas.stroke(0);
    canvas.point(xpos, ypos);

  }
  
  
  float getPos() {
    // Convert spos to be values between
    // 0 and the total width of the scrollbar
    return map(spos,sposmin, sposmax,0,1);
  }
  
  
} 
    */
    
    
    
    </script>
    <canvas id="sketch"> </canvas>
    <p id="label"></p>
</body>

</html>