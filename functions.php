<?php

//aktuelle URL lesen
function currURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

//URL von GET-Parametern befreien
function noparamURL($url) {
    $url = strtok($url, '?');
    return $url;
}

//GET-Parameter ändern/hinzufügen, falls noch nicht vorhanden
function changeParam() {
    $arguments = func_get_args();
    $newParams = $_GET;
    for($n = 1; $n < count($arguments); $n+=2) {
	$newParams[$arguments[$n]] = $arguments[$n+1];
    }
    return noparamURL($arguments[0]).'?'.http_build_query($newParams);
}

//GET-Parameter löschen
function unsetParam() {
    $arguments = func_get_args();
    $newParams = $_GET;
    for($n = 1; $n < count($arguments); $n+=1) {
	unset($newParams[$arguments[$n]]);
    }
    return noparamURL($arguments[0]).'?'.http_build_query($newParams);
}


function movenumbers ($ALLpersID,$ALLordern,$direct,$nowID) {
    $nowkey=array_search($nowID,$ALLpersID);
    $nowordern=$ALLordern[$nowkey];
    $nextkey=$nowkey+$direct;
    if(array_key_exists($nextkey,$ALLpersID)) {
	$nextID=$ALLpersID[$nextkey];
	$nextordern=$ALLordern[$nextkey];
    }
    else {
	$nextID=$ALLpersID[$nowkey];
	$nextordern=$ALLordern[$nowkey];
    }
    $output[]=$nowID;
    $output[]=$nextordern;
    $output[]=$nextID;
    $output[]=$nowordern;
    return $output;
}

function setLang() {
    if(!empty($_GET['lang'])) {
		$_SESSION['lang'] = $_GET['lang'];
		return $_GET['lang'];
    }
    
    elseif(!empty($_SESSION['lang'])) {
		return $_SESSION['lang'];
    }
    
    else {
		if(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2)=="de") {
			$_SESSION['lang'] = "de";
			return "de";
		}
		
		else {
			$_SESSION['lang'] = "en";
			return "en";
		}
    }
}

function mailCallback( $matches) {
    $exArray = explode('@',$matches[0]);
    $returnString = '<script type="text/javascript">Mail("';
    $returnString .= $exArray[0];
    $returnString .= '","';
    if($exArray[1] != 'buero.io') $returnString .= $exArray[1];
    $returnString .= '","");</script>';
    return $returnString;
    
}

function mailToScript($string) {
    $pattern = '/[A-Za-z0-9_-]+@[A-Za-z0-9_-]+.([A-Za-z0-9_-][A-Za-z0-9_]+)/';
    return(preg_replace_callback($pattern, 'mailCallback', $string, -1 ) );
}

function requireSSL() {
	if($_SERVER["HTTPS"] != "on") {
		header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
		exit();
	}
}

?>
