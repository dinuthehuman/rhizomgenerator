<?php

/********************
 * Pfad-Angaben
********************/

//Galerie-Software, Pfad relativ zu document root / Domain, '/' am Anfang, nicht am Ende
$smh5_config['path']['soft'] = '/generator/software/smh5_class';

//Bilder-Ordner, Pfad relativ zu document root / Domain, '/' am Anfang, nicht am Ende
$smh5_config['path']['pics'] = '/generator/front/smh5';



/********************
 * MySQL-database
********************/

//Host
$smh5_config['db']['host'] = 'db_host';
//User
$smh5_config['db']['user'] = 'db_user';
//Passwort
$smh5_config['db']['pass'] = 'db_pwd';
//Datenbank
$smh5_config['db']['db'] = 'generatordb';

//Tabelle, sollte nicht geändert werden müssen - wird vom create-Script übernommen
$smh5_config['db']['table'] = 'smh5_gal';



/********************
 * Text-Elemente
********************/

//Nur relevant für Galerien mit Textelementen, jede Sprache ergibt eine Array-Komponente, zweistellige Codes
//Wird auch vom create-Script angewendet
$smh5_config['lang'][] = 'de';

//Sprachen-Fallback, falls undefinierit
$smh5_config['langfall'] = 'de';

//Overlay: Text anzeigen
$smh5_config['text']['overlay'] = FALSE;

//Slider: Text anzeigen
$smh5_config['text']['slider'] = FALSE;



/********************
 * Bildgrössen
********************/
//Alle Grössen als INT ohne Masseinheit

//Thmumbnails Breite
$smh5_config['size']['thumbs']['w'] = 500;
//Thmumbnails Höhe
$smh5_config['size']['thumbs']['h'] = 500;

//Embed-Bildermittelgross, max. Breite
$smh5_config['size']['resz']['w'] = 1240;
//Embed-Bildermittelgross, max. Höhe
$smh5_config['size']['resz']['h'] = 1748;

//Overlay-Bildermittelgross, max. Breite
$smh5_config['size']['over']['w'] = 1920;
//Overlay-Bildermittelgross, max. Höhe
$smh5_config['size']['over']['h'] = 1080;


/********************
 * Backend
********************/

//Textfelder anzeigen
$smh5_config['backend']['textareas'] = FALSE;



/********************
 * verschiedenes
********************/

//absoluter Pfad document root für PHP
$smh5_config['path']['root'] = $_SERVER['DOCUMENT_ROOT'];

//absolute Domain für HTML
$pageURL = 'http';
if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
$pageURL .= "://".$_SERVER["SERVER_NAME"];
$smh5_config['path']['domain'] = $pageURL;

//Variable um config-include zu testen (Backend)
$smh5_config['test'] = TRUE;

?>



<?php

/********************
 * Galerie-Klasse
********************/

class smh5_gallery {
    
    //ID der aktuellen Galerie
    public $galid;
    
    //Array der IDs aller dazugehörigen Bilder, richtige Reihenfolge
    protected $allImgIDs;
    
    //Totale Anzahl Bilder in der Galerie
    public $total;
    
    //Array, worin alle Ausgaben gespeichert werden: thumbs, overlay, slider
    public $output;
    
    //Sprachvariable speichern
    protected $lang;
    
    //Verbindungslink MySQLi
    protected $con;
    
    //Startwert des Sliders speichern
    protected $sliderstart;
    
    
    
    #Wichtiste Variablen festlegen
    ##############################
    
    //erster Schritt: ID der Galerie festlegen, IDs aller Bilder speichern, Datenbank-Verbindung herstellen
    public function setID($id) {
	global $smh5_config;
	$this->galid = $id;
	
	//Datenbank: Verbindung herstellen
	self::connect();
	
	$sql="SELECT id FROM ".$smh5_config['db']['table']." WHERE galid='$id' ORDER BY ordern ASC";
	$result = $this->con->query($sql);
	while($row = mysqli_fetch_array($result)) {
	    $this->allImgIDs[] = $row['id'];
	}
	$this->total = count($this->allImgIDs);
    }
    
    
    //Sprach-Variable festlegen
    public function setLang($lang) {
	$this->lang = $lang;
    }
    
    
    //Output ausgeben
    public function returnOutput() {
	return $this->output;
	//MySQL-Verbindung schliessen
	self::conclose();
    }
    
    
    
    #Thumbnails
    ###########
    
    //Thumbnails noch ohne Links und JS erzeugen, speichern
    public function createThumbs() {
	global $smh5_config;
	$images = self::ImgArray('thumbs');
	for($n=0; $n <count($images); $n++) {
	    $this->output['thumbs'][$n] = "<img class='smh5thumbs' src='".$images[$n]."' />";
	}
    }
    
    
    //Thumbs mit Links versehen, die das Overlay triggern
    public function overlayThumbs() {
	$this->createThumbs();
	
	//Overlay-Funktion in Links zu Thumbs ergänzen
	for($n=0; $n < $this->total; $n++) {
	    $this->output['thumbs'][$n] = "<a href='javascript:smh5_overlay_".$this->galid."($n)'>".$this->output['thumbs'][$n]."</a>";
	}
    }
    
    
    //Thumbs mit Links versehen, die das Overlay triggern
    public function sliderThumbs() {
	$this->createThumbs();
	
	//Slider-Funktion in Links zu Thumbs ergänzen
	for($n=0; $n < $this->total; $n++) {
	    $this->output['thumbs'][$n] = "<a href='javascript:smh5_slider_".$this->galid."($n)'>".$this->output['thumbs'][$n]."</a>";
	}
    }
    
    
    
    #Overlay
    ########
    
    //Overlay (HTML, CSS, JS) erzeugen
    public function createOverlay($start = 0) {
	global $smh5_config;
	
	//PHP-Array der Bilder erstellen -> aus Ordner over
	$images = self::ImgArray('over');
	
	//Text-Array erstellen, falls nötig
	if($smh5_config['text']['overlay']) {$texts = self::TextArray();}
	
	//Javascript-Funktion definieren
	ob_start();
	?>
	    <script type="text/javascript">
	    
	    //Bilder-Array einbauen
	    <?php echo self::JSimgArray($images, 'smh5_overArray_'.$this->galid); ?>
	    
	    //Text-Array einbauen, falls nötig
	    <?php if($smh5_config['text']['overlay']) {echo self::JStextArray($texts, 'smh5_overText_'.$this->galid);} ?>
	    
	    //Speicher für Gesamtanzahl und aktuelles Bild
	    var smh5_curr_<?php echo $this->galid ?> = <?php echo $start; ?>;
	    var smh5_total_<?php echo $this->galid ?> = <?php echo $this->total -1; ?>;
	    
	    //Funktionen definieren
	    function smh5_overlay_<?php echo $this->galid ?> (start) {
		smh5_curr_<?php echo $this->galid ?> = start;
		document.getElementById("smh5_overcurrNr_<?php echo $this->galid; ?>").innerHTML = smh5_curr_<?php echo $this->galid ?>+1;
		document.getElementById("overlayImg_<?php echo $this->galid; ?>").src = smh5_overArray_<?php echo $this->galid; ?>[smh5_curr_<?php echo $this->galid ?>];
		<?php if($smh5_config['text']['overlay']) { ?>
		document.getElementById("overlayText_<?php echo $this->galid; ?>").innerHTML = smh5_overText_<?php echo $this->galid; ?>[smh5_curr_<?php echo $this->galid ?>];
		<?php } ?>
		document.getElementById("smh5_overlay_<?php echo $this->galid ?>").style.display = "block";
	    }
	    function smh5_Nooverlay_<?php echo $this->galid ?> () {
		document.getElementById("smh5_overlay_<?php echo $this->galid ?>").style.display = "none";
	    }
	    function smh5_overlayNext_<?php echo $this->galid ?> () {
		if(smh5_curr_<?php echo $this->galid ?> < smh5_total_<?php echo $this->galid ?>){smh5_curr_<?php echo $this->galid ?>++;}
		else {smh5_curr_<?php echo $this->galid ?> = 0;}
		document.getElementById("smh5_overcurrNr_<?php echo $this->galid; ?>").innerHTML = smh5_curr_<?php echo $this->galid ?>+1;
		document.getElementById("overlayImg_<?php echo $this->galid; ?>").src = smh5_overArray_<?php echo $this->galid; ?>[smh5_curr_<?php echo $this->galid ?>];
		<?php if($smh5_config['text']['overlay']) { ?>
		document.getElementById("overlayText_<?php echo $this->galid; ?>").innerHTML = smh5_overText_<?php echo $this->galid; ?>[smh5_curr_<?php echo $this->galid ?>];
		<?php } ?>
	    }
	    function smh5_overlayBef_<?php echo $this->galid ?> () {
		if(smh5_curr_<?php echo $this->galid ?> > 0){smh5_curr_<?php echo $this->galid ?>--;}
		else {smh5_curr_<?php echo $this->galid ?> = smh5_total_<?php echo $this->galid ?>;}
		document.getElementById("smh5_overcurrNr_<?php echo $this->galid; ?>").innerHTML = smh5_curr_<?php echo $this->galid ?>+1;
		document.getElementById("overlayImg_<?php echo $this->galid; ?>").src = smh5_overArray_<?php echo $this->galid; ?>[smh5_curr_<?php echo $this->galid ?>];
		<?php if($smh5_config['text']['overlay']) { ?>
		document.getElementById("overlayText_<?php echo $this->galid; ?>").innerHTML = smh5_overText_<?php echo $this->galid; ?>[smh5_curr_<?php echo $this->galid ?>];
		<?php } ?>
	    }
	    
	    </script>
	<?php
	$JS=ob_get_contents();
	ob_end_clean();
	
	
	//Style importieren
	ob_start();
	    include($smh5_config['path']['root'].$smh5_config['path']['soft']."/styles/overlay.php");
	$style = ob_get_contents();
	ob_end_clean();
	
	
	//HTML erzeugen
	ob_start(); ?>
	
	    <div id="smh5_overlay_<?php echo $this->galid; ?>">
		<div class="overlayImgContainer">
		    <img id="overlayImg_<?php echo $this->galid; ?>" src="<?php echo $images[$start]; ?>" />
		</div>
		<div class="overfaceContainer">
		    <table class="smh5_overface">
			<tr>
			    <td class="overface_links">
				<span id="smh5_overcurrNr_<?php echo $this->galid; ?>"><?php echo $start+1; ?></span> / <?php echo $this->total;?>
			    </td>
			    <td class="overface_zentral">
				<a href="javascript: smh5_overlayBef_<?php echo $this->galid ?> ();"><img src="<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft']."/icons/overlay/overbef.png"; ?>" /></a> 
				<a href="javascript: smh5_overlayNext_<?php echo $this->galid ?> ();"><img src="<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft']."/icons/overlay/overnext.png"; ?>" /></a>
			    </td>
			    <td class="overface_rechts">
				<a href="javascript: smh5_Nooverlay_<?php echo $this->galid; ?>();"><img src="<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft']."/icons/overlay/overclose.png"; ?>" /></a>
			    </td>
			</tr>
		    </table>
		</div>
		<?php if($smh5_config['text']['overlay']) { ?>
		<div class="overlayTextContainer">
		    <div id="overlayText_<?php echo $this->galid; ?>"><?php echo $texts[$start]; ?></div>
		</div>
		<?php } ?>
	    </div>
	
	<?php $html = ob_get_contents();
	ob_end_clean();
	
	//Alles zusammen in $output schreiben
	$this->output['overlay'] = $JS.$style.$html;
    }
    
    
    
    #Slider
    #######
    
    public function createSlider($start = 0) {
	global $smh5_config;
	
	$this->sliderstart = $start;
	
	//PHP-Array der Bilder erstellen -> aus Ordner over
	$images = self::ImgArray('resz');
	
	//Text-Array erstellen, falls nötig
	if($smh5_config['text']['slider']) {$texts = self::TextArray();}
	
	//Javascript-Funktion definieren
	ob_start();
	?>
	    <script type="text/javascript">
		
	    //Bilder-Array einbauen
	    <?php echo self::JSimgArray($images, 'smh5_reszArray_'.$this->galid); ?>
	    
	    //Text-Array einbauen, falls nötig
	    <?php if($smh5_config['text']['slider']) {echo self::JStextArray($texts, 'smh5_reszText_'.$this->galid);} ?>
	    
	    //Speicher für Gesamtanzahl und aktuelles Bild
	    var smh5_currResz_<?php echo $this->galid ?> = <?php echo $start; ?>;
	    var smh5_totalResz_<?php echo $this->galid ?> = <?php echo $this->total -1; ?>;
	    
	    function smh5_slider_<?php echo $this->galid ?> (start) {
		smh5_currResz_<?php echo $this->galid ?> = start;
		document.getElementById("smh5_reszcurrNr_<?php echo $this->galid; ?>").innerHTML = smh5_currResz_<?php echo $this->galid ?>+1;
		document.getElementById("smh5_reszImg_<?php echo $this->galid; ?>").src = smh5_reszArray_<?php echo $this->galid; ?>[smh5_currResz_<?php echo $this->galid ?>];
		<?php if($smh5_config['text']['overlay']) { ?>
		document.getElementById("sliderText_<?php echo $this->galid; ?>").innerHTML = smh5_reszText_<?php echo $this->galid; ?>[smh5_currResz_<?php echo $this->galid ?>];
		<?php } ?>
	    }
	    function smh5_sliderNext_<?php echo $this->galid ?> () {
		if(smh5_currResz_<?php echo $this->galid ?> < smh5_totalResz_<?php echo $this->galid ?>){smh5_currResz_<?php echo $this->galid ?>++;}
		else {smh5_currResz_<?php echo $this->galid ?> = 0;}
		document.getElementById("smh5_reszcurrNr_<?php echo $this->galid; ?>").innerHTML = smh5_currResz_<?php echo $this->galid ?>+1;
		document.getElementById("smh5_reszImg_<?php echo $this->galid; ?>").src = smh5_reszArray_<?php echo $this->galid; ?>[smh5_currResz_<?php echo $this->galid ?>];
		<?php if($smh5_config['text']['overlay']) { ?>
		document.getElementById("sliderText_<?php echo $this->galid; ?>").innerHTML = smh5_reszText_<?php echo $this->galid; ?>[smh5_currResz_<?php echo $this->galid ?>];
		<?php } ?>
	    }
	    function smh5_sliderBef_<?php echo $this->galid ?> () {
		if(smh5_currResz_<?php echo $this->galid ?> > 0){smh5_currResz_<?php echo $this->galid ?>--;}
		else {smh5_currResz_<?php echo $this->galid ?> = smh5_totalResz_<?php echo $this->galid ?>;}
		document.getElementById("smh5_reszcurrNr_<?php echo $this->galid; ?>").innerHTML = smh5_currResz_<?php echo $this->galid ?>+1;
		document.getElementById("smh5_reszImg_<?php echo $this->galid; ?>").src = smh5_reszArray_<?php echo $this->galid; ?>[smh5_currResz_<?php echo $this->galid ?>];
		<?php if($smh5_config['text']['overlay']) { ?>
		document.getElementById("sliderText_<?php echo $this->galid; ?>").innerHTML = smh5_reszText_<?php echo $this->galid; ?>[smh5_currResz_<?php echo $this->galid ?>];
		<?php } ?>
	    }
	    
	    </script>
	<?php
	$JS=ob_get_contents();
	ob_end_clean();
	
	//Bildhöhe höchstes Bild speichern
	$topHeight = self::topHeight($images);
	
	//style sheet importieren
	ob_start();
	    include($smh5_config['path']['root'].$smh5_config['path']['soft']."/styles/slider.php");
	$style = ob_get_contents();
	ob_end_clean();
	
	//HTML erzeugen
	ob_start();
	?>
	<div id="smh5_slider_<?php echo $this->galid; ?>">
	    <div id="smh5_center_<?php echo $this->galid; ?>">
		<!--PLATZHALTER:OVERLAY-TRIGGER1--><img id="smh5_reszImg_<?php echo $this->galid; ?>" src="<?php echo $images[$start]; ?>"/><!--PLATZHALTER:OVERLAYTRIGGER2-->
	    </div>
	    <!--PLATZHALTER:SLIDER-INTERFACE-->
	    <?php if($smh5_config['text']['slider']){ ?>
	    <div id="sliderText_<?php echo $this->galid; ?>"><?php echo $texts[$start]; ?></div>
	    <?php } ?>
	</div>
	
	<?php
	$html = ob_get_contents();
	ob_end_clean();
	$this->output['slider'] = $JS.$style.$html;
    }
    
    
    public function interfaceSlider($start = 0) {
	global $smh5_config;
	if(empty($this->output['slider'])) {$this->createSlider($start);}
	
	ob_start();
	?>
	<table id="smh5_sliderface_<?php echo $this->galid; ?>" >
	    <tr>
		<td class="sliderface_links">
		    
		</td>
		<td class="sliderface_zentral">
		    <a href="javascript: smh5_sliderBef_<?php echo $this->galid ?> ();"><img src="<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft']."/icons/slider/bef.png"; ?>" /></a> 
		    <a href="javascript: smh5_sliderNext_<?php echo $this->galid ?> ();"><img src="<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft']."/icons/slider/next.png"; ?>" /></a>
		</td>
		<td class="sliderface_rechts">
		    <span id="smh5_reszcurrNr_<?php echo $this->galid; ?>"><?php echo $this->sliderstart+1; ?></span> / <?php echo $this->total;?>
		</td>
	    </tr>
	</table>
	<?php
	$interface = ob_get_contents();
	ob_end_clean();
	$this->output['slider'] = str_replace('<!--PLATZHALTER:SLIDER-INTERFACE-->',$interface,$this->output['slider']);
    }
    
    
    public function overlaySlider($start = 0) {
	if(empty($this->output['slider'])) {$this->createSlider($start);}
	
	$linkpart1 = "<a href='javascript:smh5_overlay_".$this->galid."(smh5_currResz_".$this->galid.")'>";
	$linkpart2 = "</a>";
	$this->output['slider'] = str_replace('<!--PLATZHALTER:OVERLAY-TRIGGER1-->',$linkpart1,str_replace('<!--PLATZHALTER:OVERLAYTRIGGER2-->',$linkpart2,$this->output['slider']));
    }
    
    
    
    #Backend ausgeben
    #################
    
    public function createBackend() {
	
	global $smh5_config;
	ob_start();
	
	//actions ausführen
	switch($_GET['action']) {
	    
	    //Texteingaben bearbeiten
	    case "modifiy";
		
		$post_keys = array_keys($_POST);
		
		foreach($post_keys as $value) {
		    $keyparts = explode('_',$value);
		    if($keyparts[0]=='text') {
			$sql="
			    UPDATE ".$smh5_config['db']['table']."
			    SET text_".$keyparts[1]."='".self::script_nl2br(htmlspecialchars($_POST[$value], ENT_QUOTES))."'
			    WHERE id='".$keyparts[2]."'";
			$result = $this->con->query($sql);
		    }
		}
		break;
	    
	    case "delete";
		
		$sql="SELECT dateiname,base_ext FROM ".$smh5_config['db']['table']." WHERE id='".$_GET['delid']."'";
		$result = $this->con->query($sql);
		$row = mysqli_fetch_array($result);
		
		$delfile = $smh5_config['path']['root'].$smh5_config['path']['pics']."/thumbs/".$row['dateiname'].".jpg";
		if(is_file($delfile)) {unlink($delfile);}
		
		$delfile = $smh5_config['path']['root'].$smh5_config['path']['pics']."/resz/".$row['dateiname'].".jpg";
		if(is_file($delfile)) {unlink($delfile);}
		
		$delfile = $smh5_config['path']['root'].$smh5_config['path']['pics']."/over/".$row['dateiname'].".jpg";
		if(is_file($delfile)) {unlink($delfile);}
		
		$delfile = $smh5_config['path']['root'].$smh5_config['path']['pics']."/".$row['dateiname'].$row['base_ext'];
		if(is_file($delfile)) {unlink($delfile);}
		
		$sql = "DELETE FROM ".$smh5_config['db']['table']." WHERE id='".$_GET['delid']."'";
		$this->con->query($sql);
		
		break;
	    
	    case "moveup":
	    case "movedown":
		
		$sql="SELECT ordern FROM ".$smh5_config['db']['table']." WHERE id='".$_GET['moveid']."'";
		$result = $this->con->query($sql);
		$row = mysqli_fetch_array($result);
		$akt_ordern = $row['ordern'];
		
		$sql="SELECT id,ordern FROM ".$smh5_config['db']['table']." WHERE galid = '".$this->galid."' AND ordern";
		
		if($_GET['action']=='movedown') {
		    $sql .= " > $akt_ordern ORDER BY ordern ASC";
		}
		else {
		    $sql .= " < $akt_ordern ORDER BY ordern DESC";
		}
		$result = $this->con->query($sql);
		
		if(mysqli_num_rows($result) > 0) {
		    $row = mysqli_fetch_array($result);
		    $nextID = $row['id'];
		    $nextOrdern = $row['ordern'];
		    
		    $sql="UPDATE ".$smh5_config['db']['table']." SET ordern='$nextOrdern' WHERE id='".$_GET['moveid']."'";
		    $result = $this->con->query($sql);
		    $sql="UPDATE ".$smh5_config['db']['table']." SET ordern='$akt_ordern' WHERE id='$nextID'";
		    $result = $this->con->query($sql);
		}
	}
	
	//Styles importieren
	include($smh5_config['path']['root'].$smh5_config['path']['soft'].'/styles/backend.php');
	?>
	
	<iframe id="uploader" src="<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft'].'/uploader/form.php?galid='.$this->galid; ?>"></iframe>
	<input type="button" id="refresh-button" value="Ansicht aktualisieren" onClick="document.location.reload(true)" />
	<div id="backend-content"><a name="imgTable" />
	    
	    <?php
	    
	    //MySQL: relevante Bilder auswählen
	    $sql="SELECT * FROM smh5_gal WHERE galid='".$this->galid."' ORDER BY ordern ASC";
	    $result = $this->con->query($sql);
	    if(mysqli_num_rows($result) > 0) {$bilder_vorhanden = TRUE;}
	    
	    while($row = mysqli_fetch_array($result)) {
		
		//Form-Tag öffnen
		if($smh5_config['backend']['textareas']) {
		    ?><form method="post" action="<?php echo smh5_gallery::changeParam(smh5_gallery::currURL(), "action", "modifiy"); ?>#imgTable"><?php
		}
		
		//Tabelle für jedes Bild schreiben
		?>
		<div class="graueLinie"></div>
		<table class="eachIMG"><tr>
			
			<td class="IMGcont">
			    <img src="<?php echo $smh5_config['path']['domain'].$smh5_config['path']['pics'].'/thumbs/'.$row['dateiname'].'.jpg';?>" />
			    
			    <?php if($smh5_config['backend']['textareas']) { ?>
			    <table class="smh5buttons">
				<tr>
				    <td><a href="<?php echo smh5_gallery::changeParam(smh5_gallery::currURL(), "action", "delete", "delid", $row['id']); ?>#imgTable" ><div class="smh5delete" title="Bild löschen"></div></a></td>
				    <td><a href="<?php echo smh5_gallery::changeParam(smh5_gallery::currURL(), "action", "moveup", "moveid", $row['id']); ?>#imgTable"><div class="smh5up" title="Nach oben verschieben"></div></a></td>
				    <td><a href="<?php echo smh5_gallery::changeParam(smh5_gallery::currURL(), "action", "movedown", "moveid", $row['id']); ?>#imgTable"><div class="smh5down" title="Nach unten verschieben"></div></a></td>
				</tr>
			    </table>
			    <?php } ?>
			    
			</td>
			
			<?php if(!$smh5_config['backend']['textareas']) { ?>
			<td class="BUTTONcont">
			    <table class="smh5buttons">
				<tr>
				    <td><a href="<?php echo smh5_gallery::changeParam(smh5_gallery::currURL(), "action", "delete", "delid", $row['id']); ?>#imgTable" ><div class="smh5delete" title="Bild löschen"></div></a></td>
				    <td><a href="<?php echo smh5_gallery::changeParam(smh5_gallery::currURL(), "action", "moveup", "moveid", $row['id']); ?>#imgTable"><div class="smh5up" title="Nach oben verschieben"></div></a></td>
				    <td><a href="<?php echo smh5_gallery::changeParam(smh5_gallery::currURL(), "action", "movedown", "moveid", $row['id']); ?>#imgTable"><div class="smh5down" title="Nach unten verschieben"></div></a></td>
				</tr>
			    </table>
			    
			</td>
			<?php } else {
			    foreach ($smh5_config['lang'] as $value) {
				?>
				<td class="TEXTcont">
				    Text <?php echo $value; ?>: <br />
				    <textarea name="<?php echo 'text_'.$value.'_'.$row['id']; ?>"><?php echo htmlspecialchars_decode(self::script_br2nl($row['text_'.$value])); ?></textarea>
				</td>
				<?php
			    }
			} ?>
		</tr></table>
		
	    <?php
	    }
	    
	    //Submit-Button & Form-Tag schliessen
	    if($smh5_config['backend']['textareas']) {
		if($bilder_vorhanden) { ?>
		    <div class="graueLinie"></div>
		    <input type="submit" value="Texteingaben übernehmen" />
		<?php }	?>
		</form>
	    <?php } ?> 
	</div>
	
	<?php
	$this->output['backend'] = ob_get_contents();
	ob_end_clean();
    }
    
    
    
    #Bilder/Text-Arrays
    ###################
    
    //PHP-Array aller Bilder erstellen
    protected function ImgArray ($ordner) {
	global $smh5_config;
	for($n=0; $n < $this->total; $n++) {
	    $output[$n] = $smh5_config['path']['domain'].$smh5_config['path']['pics']."/$ordner/".$this->galid."-".$this->allImgIDs[$n].".jpg";
	}
	return $output;
    }
    
    //PHP-Array aller variablen Texte erstellen
    protected function TextArray() {
	global $smh5_config;
	if(!empty($this->lang)) {$lang = $this->lang;}
	else {$lang = $smh5_config['langfall'];}
	$n = 0;
	$result = $this->con->query("SELECT text_$lang FROM ".$smh5_config['db']['table']." WHERE galid='".$this->galid."' ORDER BY ordern ASC");
	while($row = mysqli_fetch_array($result)) {
	    $output[$n] = $row['text_'.$lang];
	    $n++;
	}
	return $output;
    }
    
    //Höchste Bildhöhe in Array (URLs zu JPEGs) suchen
    protected function topHeight($array) {
	foreach ($array as $value) {
	    list($w,$h[]) = getimagesize($value);
	}
	return max($h);
    }
    
    //Preload-BilderArray aus PHP-Array schreiben
    protected function JSimgArray($array, $arrayName) {
	$output = "var $arrayName = [];
	";
	$output .= "var ".$arrayName."_pre".$this->galid." = [];
	";
	for($n=0; $n < count($array); $n++) {
	    $output .= $arrayName."_pre".$this->galid."[$n] = new Image();
	    ";
	    $output .= $arrayName."_pre".$this->galid."[$n].src = '".$array[$n]."';
	    ";
	    $output .= $arrayName."[$n] = '".$array[$n]."';
	    ";
	}
	return $output;
    }
    
    //JS-Textarray aus PHP-Array schreiben
    protected function JStextArray($array, $arrayName) {
	$output = "var $arrayName = [];
	";
	for($n=0; $n < count($array); $n++) {
	    $output .= $arrayName."[$n] = '".$array[$n]."';
	    ";
	}
	return $output;
    }
    
    
    
    #MySQL-Funktionen
    #################
    
    //Verbindung herstellen
    public function connect() {
	global $smh5_config;
	$con = new mysqli($smh5_config['db']['host'],$smh5_config['db']['user'],$smh5_config['db']['pass'],$smh5_config['db']['db']);
	$con->set_charset("utf8");
	$this->con = $con;
    }
    
    //Verbindung schliessen
    public function conclose() {
	$this->con->close();
    }
    
    //Tabelle erstellen
    public function createTable() {
	global $smh5_config;
	foreach($smh5_config['lang'] as $value) {
	    $sql2 .= ", text_$value TEXT NOT NULL";
	}
	
	$sql =
	"CREATE TABLE IF NOT EXISTS ".$smh5_config['db']['table']."
	(
	    id INT NOT NULL AUTO_INCREMENT,
	    galid INT NOT NULL,
	    ordern INT NOT NULL,
	    dateiname VARCHAR(150) NOT NULL,
	    base_ext VARCHAR (8) NOT NULL $sql2,
	    PRIMARY KEY (id)
	)
	CHARACTER SET utf8
	COLLATE utf8_general_ci
	";
	
	$result = $this->con->query($sql);
	
	if(!$result) {
	    echo 'Fehler: '.$this->con->mysqli_error();
	}
	else {
	    echo 'Tabelle erfolgreich erstellt';
	}
	
	echo '<br />Sprachspalten: ';
	foreach($smh5_config['lang'] as $value) {
	    echo "$value, ";
	}
    }
    
    
    
    #Verschiedenes
    ##############
    
    //aktuelle URL lesen
    protected function currURL() {
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];}
	else {$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];}
	return $pageURL;
    }
    
    //URL von GET-Parametern befreien
    protected function noparamURL($url) {
	$url = strtok($url, '?');
	return $url;
    }
    
    //GET-Parameter ändern/hinzufügen, falls noch nicht vorhanden
    protected function changeParam() {
	$arguments = func_get_args();
	$newParams = $_GET;
	for($n = 1; $n < count($arguments); $n+=2) {$newParams[$arguments[$n]] = $arguments[$n+1];}
	return smh5_gallery::noparamURL($arguments[0]).'?'.http_build_query($newParams);
    }
    
    //GET-Parameter löschen
    protected function unsetParam() {
	$arguments = func_get_args();
	$newParams = $_GET;
	for($n = 1; $n < count($arguments); $n+=1) {unset($newParams[$arguments[$n]]);}
	return smh5_gallery::noparamURL($arguments[0]).'?'.http_build_query($newParams);
    }
    
    //JavaScript-fähiges nl2br
    protected function script_nl2br($arg) {
	$arg= str_replace("\r\n", "<br />", $arg);
	$arg= str_replace("\n", "<br />", $arg);
	$arg= str_replace("\r", "<br />", $arg);
	return $arg;
    }
    
    //Umkehrfunktion: JavaScript-fähiges nl2br
    protected function script_br2nl($arg) {
	$arg= str_replace("<br />", "\r\n", $arg);
	return $arg;
    }
    
}//Galerie-Klasse zu Ende



/********************
 * Bilderfunktionen
********************/
/**
 * Gravitywell Image
 *
 * @author Gravitywell Ltd
 * @version $Id$
 * @copyright Gravitywell Ltd, 23 November, 2009
 * @package default
 **/
 
 
class Gravitywell_Image
{
    /**
     * Image Resource
     *
     */
    protected $_image;
    
    /**
     * Image Type
     *
     */
    protected $_imageType;
    
    /**
     * Background color
     *
     */
    protected $_bgColor = array('255', '255', '255');
    
    /**
     * Border width
     *
     */
    protected $_borderWidth = 0;
    
    /**
     * Border Color
     *
     */
    protected $_borderColor = array('255', '255', '255');
    
    
    /**
     * Auto fill the image
     *
     */
    protected $_autofill = false;
    
    
    /**
     * Constructor, Can optionally take a filename for an 
     * image and automatically load it
     *
     * @param string $image 
     * @author Gravitywell Ltd
     */
    public function __construct($image = null)
    {
        if(!is_null($image)) {
            $this->load($image);
        }
    }
    
    
    /**
     * Loads the image and stores it
     *
     * @param string $filename 
     * @return void
     * @author Gravitywell Ltd
     */
    public function load($filename)
    {
        if(!is_file($filename)) {
            throw new Zend_Exception("Gravitywell_Image::load: file doesn't exist or is directory.");
        }
        
        // Get some image information
        list($w, $h, $this->_imageType) = getimagesize($filename);
        
        // Switch on the image type and load the image
        switch($this->_imageType) {
            // JPEG
            case IMAGETYPE_JPEG: 
                $this->_image = imagecreatefromjpeg($filename);
                break;
            
            // GIF
            case IMAGETYPE_GIF:
                $this->_image = imagecreatefromgif($filename);
                break;
            
            // PNG
            case IMAGETYPE_PNG:
                $this->_image = imagecreatefrompng($filename);
                break;
        }
    }
    
    
    
    /**
     * Saves the image to disk
     *
     * @param string $filename 
     * @param string $filetype 
     * @param int $compression 
     * @param string $permissions 
     * @return void
     * @author Gravitywell Ltd
     */
    public function save($filename, $compression = 100, $permissions = null)
    {
        // check if image loaded
        if (empty($this->_image)) {
            //throw new Zend_Exception("Image doesn't exist.");
            return false;
        }
        
        // Add any borders we've added
        $this->_addBorders();
        
        
        ###############################
        #DINU-EDIT: alle Typen zu JPEG
        ###############################
        // Switch onthe type of image
        switch($this->_imageType) {
            case IMAGETYPE_JPEG:
            case IMAGETYPE_GIF:
            case IMAGETYPE_PNG:
                imagejpeg($this->_image, $filename, $compression);
                break;
            /*
                imagegif($this->_image, $filename);
                break;
            
                imagepng($this->_image, $filename);
                break;
            */
        }
        
        // Permissions
        if(!is_null($permissions)) {
            chmod($filename, $permissions);
        }
    }
    
    
    /**
     * Output the image directly to the browser
     *
     * @return void
     * @author Gravitywell Ltd
     */
    public function output()
    {        
        // Add any borders we've added
        $this->_addBorders();
        
        // Switch onthe type of image
        switch($this->_imageType) {
            case IMAGETYPE_JPEG:
                header('Content-type: image/jpeg');
                imagejpeg($this->_image);
                break;
            case IMAGETYPE_GIF:
                header('Content-type: image/gif');
                imagegif($this->_image);
                break;
            case IMAGETYPE_PNG:
                header('Content-type: image/png');
                imagepng($this->_image);
                break;
        }
    }
    
    
    /**
     * Returns the width of the image
     *
     * @return void
     * @author Gravitywell Ltd
     */
    public function getWidth()
    {
        return imagesx($this->_image);
    }
    
    
    /**
     * Returns the height of the image
     *
     * @return void
     * @author Gravitywell Ltd
     */
    public function getHeight()
    {
        return imagesy($this->_image);
    }
    
    
    /**
     * Resizes the image to the specified dims, does not crop any of the image
     *
     * @param string $mwidth 
     * @param string $mheight 
     * @return void
     * @author Gravitywell Ltd
     */
    public function resize($mwidth = null, $mheight = null)
    {
        if(is_null($mwidth) || is_null($mheight)) {
            throw new Zend_Exception('Gravitywell_Image::resize() requires both a max width and a max height passed.');
        }
        
        // Get the original Image ratio
        $originalRatio = $this->getWidth() / $this->getHeight();
        
        // Find the new width and height
        if ($mwidth/$mheight > $originalRatio) {
            $newHeight = $mheight;
            $newWidth = $mheight * $originalRatio;
        } else {
            $newWidth = $mwidth;
            $newHeight = $mwidth / $originalRatio;
        }
        
        // Create a blank canvas
        if($this->_autofill) {
            $newImage = imagecreatetruecolor($mwidth, $mheight);
        }
        else {
            $newImage = imagecreatetruecolor($newWidth, $newHeight);
        }
        
        // Add the background
        $bgColor = imagecolorallocate($newImage, $this->_bgColor[0], $this->_bgColor[1], $this->_bgColor[2]);
        imagefilledrectangle($newImage, 0, 0, $this->getWidth(), $this->getHeight(), $bgColor);

        
        // Copy the old image to it
        imagecopyresampled($newImage, $this->_image, 0, 0, 0, 0, $newWidth, $newHeight, $this->getWidth(), $this->getHeight());
        
        // Replace the image
        $this->_image = $newImage;
        
        return $this;
    }
    
    
    /**
     * Crops an image to a specified size, if either width or height aren't specified, crops a 1:1 AR version of it, without any white space
     *
     * @param int $x 
     * @param int $y 
     * @param int $width 
     * @param int $height 
     * @return void
     * @author Gravitywell Ltd
     */
    public function crop($width = 0, $height = 0, $x = 0, $y = 0) 
    {        
        $newImage = imagecreatetruecolor($width, $height);
        
        imagecopyresampled($newImage, $this->_image, 0, 0, $x, $y, $width, $height, $width, $height);
        
        // Replace the image
        $this->_image = $newImage;      
        
        return $this;  
    }
    
    
    /**
     * Creates a thumbnail of the image, cropping to the width and height to avoid white space
     *
     * @param int $width 
     * @param int $height 
     * @param string $x 
     * @param string $y 
     * @return void
     * @author Gravitywell Ltd
     */
    public function thumbnail($width = 100, $height = 100, $x = null, $y = null)
    {
        // Get the original Image ratio
        $originalRatio = $this->getWidth() / $this->getHeight();
        
        // Find the new width and height
        if ($width/$height > $originalRatio) {
            $newHeight = $width / $originalRatio;
            $newWidth = $width;
        } else {
            $newWidth = $height * $originalRatio;
            $newHeight = $height;
        }
        
        // If both x and y are null, center the crop
        if(is_null($x) && is_null($y)) {
            $xCenter = $newWidth / 2;
            $x = $xCenter - ($width / 2);
            $yCenter = $newHeight / 2;
            $y = $yCenter - ($height / 2);
        }
        
        if(is_null($x)) {
            $x = 0;
        }
        
        if(is_null($y)) {
            $y = 0;
        }
        
        // Resize the image
        $this->resize($newWidth, $newHeight);
        
        // Crop the image
        $this->crop($width, $height, $x, $y);
        
        return $this;
    }
    
    
    
    /**
     * Sets a background color, accepts hash or RGB comma seperated
     *
     * @param int $red 
     * @param int $green 
     * @param int $blue 
     * @return void
     * @author Gravitywell Ltd
     */
    public function setBackgroundColor($red, $green, $blue)
    {
        $this->_bgColor[0] = $red;
        $this->_bgColor[1] = $green;
        $this->_bgColor[2] = $blue;
        
        return $this;
    }
    
    
    /**
     * Adds a border to the image
     *
     * @param int $width 
     * @param int $red 
     * @param int $green 
     * @param int $blue 
     * @return void
     * @author Gravitywell Ltd
     */
    public function setBorder($width, $red, $green, $blue)
    {
        $this->_borderWidth = $width;
        $this->_borderColor[0] = $red;
        $this->_borderColor[1] = $green;
        $this->_borderColor[2] = $blue;
        
        return $this;
    }
    
    
    /**
     * Adds the borders to the image resource
     *
     * @return void
     * @author Gravitywell Ltd
     */
    public function _addBorders()
    {
        if($this->_borderWidth > 0) {
            // Rererence the width
            $border = &$this->_borderWidth;
            
            // Make the color
            $borderColor = imagecolorallocate($this->_image, $this->_borderColor[0], $this->_borderColor[1], $this->_borderColor[2]);
            
            // Gooo
            imagefilledrectangle($this->_image, 0, $this->getHeight(), $this->getWidth(), $this->getHeight() - $border, $borderColor);          //Bottom
            imagefilledrectangle($this->_image, 0, 0, $this->getWidth(), $border - 1, $borderColor);                                            //Top
            imagefilledrectangle($this->_image, 0, 0, $border - 1, $this->getHeight(), $borderColor);                                           //Left
            imagefilledrectangle($this->_image, $this->getWidth() - $border, 0, $this->getWidth(), $this->getHeight(), $borderColor);           //Right
        }
    }
    
    /**
     * Sets whether we should autofill the image or not
     *
     * @param bool $autofill 
     * @return void
     * @author Gravitywell Ltd
     */
    public function setAutofill($autofill)
    {
        $this->_autofill = $autofill;
        
        return $this;
    }
    
}
