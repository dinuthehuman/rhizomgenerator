<style type="text/css">
    
    
    iframe#uploader {
	width: 800px;
	height: 300px;
	border: none;
	margin: 0px;
	padding: 0px;
	display: block;
    }
    
    input {
	font-family: Verdana, sans-serif;
	font-size: 11px;
    }
    
    input#refresh-button {
	margin-left: 20px;
    }
    
    div#backend-content {
	margin-left: 20px;
	width: 760px;
    }
    
    div.graueLinie {
	height: 2px;
	width: 760px;
	background-color: #666;
	margin-top: 15px;
	margin-bottom: 15px;
    }
    
    table.eachIMG {
	border: none;
	border-collapse: collapse;
    }
    
    table.eachIMG td {
	border: none !important;
	font-size: 10px;
    }
    
    table.eachIMG td.IMGcont {
	width: <?php echo $smh5_config['size']['thumbs']['w']; ?>;
	padding: 0px;
    }
    
    table.eachIMG td.BUTTONcont {
	padding-left: 15px;
    }
    
    table.eachIMG td.TEXTcont {
	width: <?php echo (760-$smh5_config['size']['thumbs']['w'])/count($smh5_config['lang'])-10; ?>px;
	padding-left: 10px;
    }
    
    td.TEXTcont textarea {
	width: <?php echo (760-$smh5_config['size']['thumbs']['w'])/count($smh5_config['lang'])-20; ?>px;
	height: <?php echo $smh5_config['size']['thumbs']['h']; ?>px;
    }
    
    table.smh5buttons div {
	<?php if(!$smh5_config['backend']['textareas']) { ?>
	height: 32px;
	width: 32px;
	background-position: 0px 32px;
	<?php } else { ?>
	height: 16px;
	width: 16px;
	background-position: 0px 16px;
	<?php } ?>
    }
    
    table.smh5buttons div:hover {
	background-position: 0px 0px;
    }
    
    div.smh5delete {
	<?php if(!$smh5_config['backend']['textareas']) { ?>
	background-image: url("<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft'].'/icons/backend/delete.png'; ?>");
	<?php } else { ?>
	background-image: url("<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft'].'/icons/backend/delete16.png'; ?>");
	<?php } ?>
    }
    div.smh5up {
	<?php if(!$smh5_config['backend']['textareas']) { ?>
	background-image: url("<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft'].'/icons/backend/up.png'; ?>");
	<?php } else { ?>
	background-image: url("<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft'].'/icons/backend/up16.png'; ?>");
	<?php } ?>
    }
    div.smh5down {
	<?php if(!$smh5_config['backend']['textareas']) { ?>
	background-image: url("<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft'].'/icons/backend/down.png'; ?>");
	<?php } else { ?>
	background-image: url("<?php echo $smh5_config['path']['domain'].$smh5_config['path']['soft'].'/icons/backend/down16.png'; ?>");
	<?php } ?>
    }

</style>