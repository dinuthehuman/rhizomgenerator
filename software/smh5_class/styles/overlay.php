<?php
if($smh5_config['text']['overlay']) {
        
        #CONFIG: Höhe Text-Overlay
        $textheight_overlay = 80;
        ##########################
        
        $textOffset = $textheight_overlay+10;
}
else {
        $textOffset = 0;
        $textheight_overlay = 0;
}


?>

<style type="text/css">
        
        div#smh5_overlay_<?php echo $this->galid; ?> {
            background-color: rgba(0,0,0,0.7);
            display: none;
            position: fixed;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 0px;
            width: 100%;
            height: 100%;
            vertical-align: middle;
            text-align: center;
            color: rgb(255,255,255);
            font-size: 11px !important;
            z-index: 100;
        }
        
        div.overlayImgContainer {
                position: absolute;
                top: 15px;
                left: 0px;
                right: 0px;
                bottom: <?php echo (40+$textOffset); ?>px;
        }
        
        img#overlayImg_<?php echo $this->galid; ?> {
            margin-left: auto;
            margin-right: auto;
            text-align: center;
            max-width: 100%;
            max-height: 100%;
        }
        
        #smh5_overlay_<?php echo $this->galid; ?> a {
            color: rgb(255,255,255);
        }
        
        div.overfaceContainer {
                position: absolute;
                bottom: <?php echo (10+$textOffset); ?>px;
                width: 100%;
        }
        
        table.smh5_overface {
                border: none;
                border-collapse: collapse;
                margin-left: auto;
                margin-right: auto;
                max-width: 400px;
                width: 90vw;
                
        }
        .smh5_overface td {
                width: 33%;
        }
        .smh5_overface td.overface_links {
                text-align: left;
        }
        .smh5_overface td.overface_zentral {
                text-align: center;
        }
        .smh5_overface td.overface_rechts {
                text-align: right;
        }
        
        div.overlayTextContainer {
                height: <?php echo $textheight_overlay; ?>px;
                width: 100%;
                position: absolute;
                bottom: 10px;
        }
        
        div#overlayText_<?php echo $this->galid; ?> {
                height: <?php echo $textheight_overlay; ?>px;
                width: 400px;
                text-align: justify;
                margin-left: auto;
                margin-right: auto;
                overflow-y: auto;
        }
        
</style>