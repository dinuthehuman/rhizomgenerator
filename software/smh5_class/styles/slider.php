<style type="text/css">
    
    div#smh5_slider_<?php echo $this->galid; ?> {
	width: <?php echo $smh5_config['size']['resz']['w']; ?>px;
    }
    
    div#smh5_center_<?php echo $this->galid; ?> {
	width: inherit;
	margin: 0px;
	padding: 0px;
	height: <?php echo $topHeight; ?>px;
	text-align: center;
    }
    
    img#smh5_reszImg_<?php echo $this->galid; ?> {
	margin-left: auto;
	margin-right: auto;
    }
    
    table#smh5_sliderface_<?php echo $this->galid; ?> {
	width: inherit;
	margin-top: 10px;
	margin-bottom: 15px;
	font-size: 12px !important;
    }
    table#smh5_sliderface_<?php echo $this->galid; ?> td {
	width: 33%;
    }
    table#smh5_sliderface_<?php echo $this->galid; ?> td.sliderface_links {
	text-align: left;
    }
    table#smh5_sliderface_<?php echo $this->galid; ?> td.sliderface_zentral {
	text-align: center;
    }
    table#smh5_sliderface_<?php echo $this->galid; ?> td.sliderface_rechts {
	text-align: right;
    }
    
    div#sliderText_<?php echo $this->galid; ?> {
	margin-bottom: 15px;
	width: inherit;
	text-align: justify;
    }
</style>