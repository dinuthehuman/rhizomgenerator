<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <title>HTML5 Drag and Drop Multiple Image Uploader</title>
        <link href="main.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div class="upload_form_cont">
                <div id="dropArea">
                    <div style="height: 180px;"></div>
                    <span style="font-size: 14px; font-weight: bold;">Drop Area</span><br />
                    Bilder per Drag and Drop hochladen
                </div>

                <div class="info">
                    <div>verbleibende Dateien: <span id="count">0</span></div>
                    
                    <!--Pfad zum Upload-Script als hidden value-->
                    <input id="url" type="hidden" value="upload.php?galid=<?php echo $_GET["galid"]; ?>"/>
                    
                    <!--Fortschirtt-->
                    <canvas width="500" height="20"></canvas>
                    
                    <!--Resultat-Backchannel-->
                    <div id="result">
                        <?php if(empty($_GET['galid'])) {echo '<div class="f">Fehler: Gallery-ID nicht übergeben!</div>';} ?>
                    </div>
                    
                </div>
            </div>
        </div>
        <script src="script.js"></script>
    </body>
</html>