<?php
//Galerie-Konfiguration laden
include_once('../smh5_gallery.php');

//Konfiguration: Erlaubte Dateitypen
$allowed_types = array('image/gif','image/jpeg','image/png');

//Konfiguration: Upload-Folder
$upload_folder = $smh5_config['path']['root'].$smh5_config['path']['pics'];

//Galerie-ID aus URL
$galid = $_GET['galid'];

//MySQL-Verbindung aufbauen
$smh5_con = mysqli_connect($smh5_config['db']['host'],$smh5_config['db']['user'],$smh5_config['db']['pass'],$smh5_config['db']['db']);
if(!$smh5_con) {echo "<div class='f'>Fehler: MySQL-Verbindung</div>"; exit;}
mysqli_set_charset($smh5_con, "utf8");

// set error reporting level
if (version_compare(phpversion(), '5.3.0', '>=') == 1)
  error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
else
  error_reporting(E_ALL & ~E_NOTICE);

function bytesToSize1024($bytes, $precision = 2) {
    $unit = array('B','KB','MB');
    return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), $precision).' '.$unit[$i];
}


//Upload beginnen
if (isset($_FILES['myfile'])) {
    
    $sFileName = $_FILES['myfile']['name'];
    $sFileType = $_FILES['myfile']['type'];
    
    //Upload-Folder gefunden, weitermachen
    if(is_dir($upload_folder)) {
	
	//File Type erlaubt, weitermachen
	if(in_array($sFileType, $allowed_types)) {
	    
	    //MySQL: Dateinamen generieren
	    $ext = '.'.pathinfo($_FILES['myfile']['name'], PATHINFO_EXTENSION);
	    //nächste AI (id)
	    $sql = "SHOW TABLE STATUS LIKE '".$smh5_config['db']['table']."'";
	    $result = mysqli_query($smh5_con,$sql); $row = mysqli_fetch_array($result);
	    $nextInc = $row['Auto_increment'];
	    //nächste OrderN
	    $sql = "SELECT MAX(ordern) FROM ".$smh5_config['db']['table']." WHERE galid = $galid";
	    $result = mysqli_query($smh5_con,$sql);
	    while($row = mysqli_fetch_array($result)){$nextOrdern=$row["MAX(ordern)"]+1;}
	    if(empty($nextOrdern)) {$nextOrdern = 1;}
	    //Dateinamen (ohne Endung) zusammensetzen
	    $newname = $galid.'-'.$nextInc;
	    
	    //Datei verschieben, umbenennen
	    $is_up = move_uploaded_file( $_FILES['myfile']['tmp_name'] , $upload_folder.'/'.$newname.$ext);
	    
	    //Datei ist platziert, Datenbankeintrag schreiben
	    if($is_up) {
		$sql="INSERT INTO ".$smh5_config['db']['table']." (galid, ordern, dateiname, base_ext) VALUES ('$galid','$nextOrdern','$newname','$ext')";
		mysqli_query($smh5_con, $sql);
		
		//Ordner checken
		if(!is_dir($upload_folder.'/thumbs')) {mkdir($upload_folder.'/thumbs');}
		if(!is_dir($upload_folder.'/resz')) {mkdir($upload_folder.'/resz');}
		if(!is_dir($upload_folder.'/over')) {mkdir($upload_folder.'/over');}
		
		//Thumbnail generieren
		$image = new Gravitywell_Image();
		$image->load($upload_folder.'/'.$newname.$ext);
		$image->thumbnail($smh5_config['size']['thumbs']['w'], $smh5_config['size']['thumbs']['h']);
		$image->save($upload_folder.'/thumbs/'.$newname.'.jpg');
		
		//mittelgrosse Version erstellen
		$image = new Gravitywell_Image();
		$image->load($upload_folder.'/'.$newname.$ext);
		$image->thumbnail($smh5_config['size']['resz']['w'], $smh5_config['size']['resz']['h']);
		$image->save($upload_folder.'/resz/'.$newname.'.jpg');
		
		//grosse Version erstellen
		$image = new Gravitywell_Image();
		$image->load($upload_folder.'/'.$newname.$ext);
		$image->resize($smh5_config['size']['over']['w'], $smh5_config['size']['over']['h']);
		$image->save($upload_folder.'/over/'.$newname.'.jpg');
		
		//Erfolg-Meldung ausgeben
		echo "<div class='s'><p>$sFileName erfolgreich hochgeladen</p></div>";
	    }
	    
	    //Verschieben/umbenennen gescheitert
	    else {
		echo "<div class='f'><p>Fehler: move_uploaded_file == false</p></div>";
	    }
	    
	}
	
	//Datei-Typ nicht akzeptiert
	else {
	    echo "<div class='f'>Fehler: $sFileName: Dateityp ungültig</div>";
	}
	
    }
    
    //Upload-Ordner nicht gefunden
    else {
	echo '<div class="f">Fehler: Upload-Folder nicht gefunden</div>';
    }
    
}

else {
    echo '<div class="f">Fehler: $_FILES-Array undefiniert</div>';
}

mysqli_close($smh5_con);

?>